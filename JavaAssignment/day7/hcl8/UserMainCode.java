/*
 * @author Aryan_Gautam
 *   
 * WAP to read two arraylist of length 5 ,  sort the list ,merge them and in third list keep
 * the integer of index 2 , 6 and 8 then display list3.
 * 
 */

package hcl8;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;

public class UserMainCode {

	public static void main(String[] args) {
		System.out.println("Enter list 1");

		ArrayList<Integer> list1 = new ArrayList<Integer>();
		ArrayList<Integer> list2 = new ArrayList<Integer>();
		Scanner input = new Scanner(System.in);
		for (int i = 0; i < 5; i++)
			list1.add(input.nextInt());
		System.out.println("Enter list 2");
		for (int i = 0; i < 5; i++)
			list2.add(input.nextInt());
		     input.close();
		System.out.println(sortMergedArrayList(list1, list2));
	}

	public static ArrayList<Integer> sortMergedArrayList(ArrayList<Integer> list1, ArrayList<Integer> list2) {

		list1.addAll(list2);
		Collections.sort(list1);
		ArrayList<Integer> list3 = new ArrayList<Integer>();
		list3.add(list1.get(2));
		list3.add(list1.get(6));

		list3.add(list1.get(8));

		return list3;
	}

}
/*
 * @author Aryan Gautam 
 * 
 *WAP to print valid if first and last character of a string is same, else invalid.
 */

package hcl7;

import java.util.Scanner;

public class UserMainCode {

	public static void main(String[] args) {
		System.out.println("Enter a String");
		Scanner s = new Scanner(System.in);
		String input = s.nextLine();
		s.close();
		System.out.println(checkCharacter(input));

	}

	public static String checkCharacter(String input) {
		int index = input.length() - 1;
		if (input.charAt(0) == input.charAt(index)) {
			return "valid";
		} else
			return "invalid";

	}
}

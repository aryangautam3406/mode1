/*
 * @author Aryan Gautam
 * Write a Java program to extract date, time from the date string
 */





package hcl2;

import java.util.*;
import java.text.*;

public class DateAndTime {

	public static void main(String[] args) {
		String originalString = "2021-05-06 09:00:02";
		Date date;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(originalString);

			String newstr = new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss").format(date);
			System.out.println("\n" + newstr + "\n");
		} catch (ParseException e) {

			e.printStackTrace();
		}
	}

}

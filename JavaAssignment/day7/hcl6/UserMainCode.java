/*
 * @author Aryan_Gautam
 * The first input corresponds to the year and the second input corresponds to 
 *  the month code.
 *  The method returns an integer corresponding to the number of days in the month
 * 
 */



package hcl6;

import java.util.Calendar;
import java.util.Scanner;

public class UserMainCode {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int year = sc.nextInt();
		int month = sc.nextInt();
		System.out.println(getNumberOfDays(year, month-1));
		sc.close();

	}

	public static int getNumberOfDays(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set((Calendar.MONTH), month);
		int DAY_OF_MONTH = cal.getActualMaximum((cal.DAY_OF_MONTH));
			return DAY_OF_MONTH;
	}

}

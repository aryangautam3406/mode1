/*
 * @author Aryan Gautam
 * 
 * Write a java program to print current date and time in the specified format. 
 */


package hcl1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateAndTimeMain {

	public static void main(String[] args) {
		DateTimeFormatter dtf=DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime dateTime=LocalDateTime.now() ;
		System.out.println(dtf.format(dateTime));

	}

}

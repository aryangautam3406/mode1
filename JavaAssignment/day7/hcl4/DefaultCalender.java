/*
 * @author Aryan_Gautam
 * Write a Java program to get the maximum value of the year, month, week, date 
 * from the current date of a default calendar
 * 
 */

package hcl4;

import java.util.*;

public class DefaultCalender {

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
	
		System.out.println("\nCurrent Date and Time:" + cal.getTime());
		int actualMaxYear = cal.getActualMaximum(Calendar.YEAR);
		int actualMaxMonth = cal.getActualMaximum(Calendar.MONTH);
		int actualMaxWeek = cal.getActualMaximum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = cal.getActualMaximum(Calendar.DATE);

		System.out.println("Actual Maximum Year: " + actualMaxYear);
		System.out.println("Actual Maximum Month: " + (actualMaxMonth + 1));
		System.out.println("Actual Maximum Week of Year: " + actualMaxWeek);
		System.out.println("Actual Maximum Date: " + actualMaxDate + "\n");
	

	}

}

/*
 * @author Aryan_Gautam
 * 
 * Given two inputs year and month (Month is coded as Jan=0, Feb=1, Mar=2 �),
 *  write a program to find out the total number of days in the given month for the given year.
 * 
 */

package hcl5;

import java.util.Calendar;
import java.util.Scanner;

public class MaximumDays {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int year = sc.nextInt();
		int month = sc.nextInt();
		System.out.println(display(year, month));
		sc.close();

	}

	public static int display(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		int DAY_OF_MONTH = cal.getActualMaximum(cal.DAY_OF_MONTH);
			return DAY_OF_MONTH;
	}

	
}

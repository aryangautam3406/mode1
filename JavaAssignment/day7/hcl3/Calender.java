/* @author Aryan Gautam
 *  // Get and display information of current date from the calendar
 */



package hcl3;
import java.util.*;
public class Calender {

	public static void main(String[] args) {
		
        Calendar cal = Calendar.getInstance();
      
	  
        System.out.println("Year: " + cal.get(Calendar.YEAR));
        System.out.println("Month: " + (cal.get(Calendar.MONTH )+1));
        System.out.println("Day: " + cal.get(Calendar.DATE));
        System.out.println("Hour: " + cal.get(Calendar.HOUR));
        System.out.println("Minute: " + cal.get(Calendar.MINUTE));
	    System.out.println();

	}

}

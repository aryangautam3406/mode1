package hcl_14;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserMainCode {
public static void covertDateFormat(String dob) throws ParseException
{
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");

	Date date = formatter.parse(dob);
	System.out.println(new SimpleDateFormat("dd/MM/yy").format(date));
	
}
}

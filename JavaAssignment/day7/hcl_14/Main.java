/*
 * @author Aryan_Gautam
 * WAP to parse Input date(dd-mm-yyyy) to (dd-mm-yy).
 * 
 * 
 */

package hcl_14;

import java.text.ParseException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws ParseException {
		System.out.println("Enter your date of birth (dd-MM-YYyy): ");
		Scanner sc = new Scanner(System.in);
		String dob = sc.nextLine();
		UserMainCode.covertDateFormat(dob);
		sc.close();
		
		

	}
}

/*
 * @author Aryan_Gautam
 * Write a Java program by using BufferReader class to 
 * prompt user to input his/her name and then the output will be shown  
 * 
 */

package hcl1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BufferReaderMain {

	public static void main(String[] args) {
		BufferedReader reader =new BufferedReader(new InputStreamReader(System.in));
	      System.out.println("Enter your name: ");
	      try {
			String name = reader.readLine();
			System.out.println(name);
		} catch (IOException e) {
			
			e.printStackTrace();
		}

	}

}

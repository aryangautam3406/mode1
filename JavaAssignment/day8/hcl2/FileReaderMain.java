/*
 * @author Aryan_Gautam
 * Write a Java program to write and read a plain text file
 * 
 */


package hcl2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
public class FileReaderMain {

	public static void main(String args[]) {
		
	        StringBuilder sb = new StringBuilder();
	        String strLine = "";
	        try
	          {
	             String filename= "C:\\java/myfile.txt";
	             FileWriter fw = new FileWriter(filename,false); 
	             //appends the string to the file
	             fw.write("java Exercises \n");
	             		
	             fw.close();
	             BufferedReader br = new BufferedReader(new FileReader("C:\\java/myfile.txt"));
	             //read the file content
	             while (strLine != null)
	             {
	                sb.append(strLine);
	                sb.append(System.lineSeparator());
	                strLine = br.readLine();
	                System.out.println(strLine);
	            }
	             br.close();                          
	           }
	           catch(IOException ioe)
	           {
	            System.err.println("IOException: " + ioe.getMessage());

	}

}
}

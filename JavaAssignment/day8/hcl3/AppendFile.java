/*
 * @author Aryan_Gautam
 * Write a Java program to append text to an existing file.
 * 
 */



package hcl3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class AppendFile {

	public static void main(String[] args) {
		  String path = System.getProperty("C:") + "C:\\java\\myfile.txt";
	        String text = "Append";
	        StringBuilder sb = new StringBuilder();
	        String strLine = "";

	        try {
	            FileWriter fw = new FileWriter("C:\\java\\myfile.txt", true);
	            fw.write(text);
	            fw.close();
	            BufferedReader br = new BufferedReader(new FileReader("C:\\java/myfile.txt"));
	             //read the file content
	             while (strLine != null)
	             {
	                sb.append(strLine);
	                sb.append(System.lineSeparator());
	                strLine = br.readLine();
	                System.out.println(strLine);
	            }
	             br.close();      
	        }
	        catch(IOException e) {
	        }

	}

}

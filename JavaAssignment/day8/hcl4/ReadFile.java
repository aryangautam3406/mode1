/*
 * @author Aryan_Gautam
 * Write a Java program to read first 3 lines from a file. 
 * 
 * 
 */


package hcl4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {

	public static void main(String[] args) {
		 StringBuilder sb = new StringBuilder();
	        String strLine = "";
	        try
	          {
		 BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\owner's\\Desktop/sql commands.txt"));
         //read the file content
         while (strLine != null)
         {
            sb.append(strLine);
            sb.append(System.lineSeparator());
            strLine = br.readLine();
            System.out.println(strLine);
        }
         br.close();   
	          }
	           catch(IOException ioe)
	           {
	            System.err.println("IOException: " + ioe.getMessage());

	}

	}

}

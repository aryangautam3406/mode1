/*
 * @author Aryan Gautam
 * Rewrite the earlier program so that, 
 * now the class DemoThread1 instead of implementing from Runnable interface,
 *  will now extend from Thread class. 
 * 
 * 
 * 
 */

package hcl4;


public class DemoThread extends Thread{

	public void run() {
		System.out.println("running child Thread in loop :");
		for (int i = 0; i <= 9; i++) {
			System.out.println(i);

			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

	public static void main(String args[]) {
		DemoThread m1 = new DemoThread();
		Thread t1 = new Thread(m1);
		t1.start();
	}
}

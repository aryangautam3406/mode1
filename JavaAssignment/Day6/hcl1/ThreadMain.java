/*
 * @author Aryan Gautam
 * 
 * Write a program to assign the current thread to t1.
 *  Change the name of the thread to MyThread.
 *   Display the changed name of the thread. Also it should display the current time.
 *  Put the thread to sleep for 10 seconds and 
 *  display the time again. 
 */



package hcl1;

public class ThreadMain {
	  public static void main (String[] args) 
	    {
	        
	        ThreadNaming t1 = new ThreadNaming();
	        ThreadNaming t2 = new ThreadNaming();
	          
	        // getting the above created threads names.
	        System.out.println("Thread 1: " + t1.getName());
	        System.out.println("Thread 2: " + t2.getName());
	          
	        t1.start();
	        t2.start();
	          
	        
	        t1.setName("Change1");
	        t2.setName("Change2");
	          
	        
	        System.out.println("Thread names after changing the "+ 
	        "thread names");
	        System.out.println("New Thread 1 name:  " + t1.getName());
	        System.out.println("New Thread 2 name: " + t2.getName());
	          
	    }
	}


package hcl1;

import java.time.LocalDateTime;

class ThreadNaming extends Thread 
	{
	      
	    @Override
	    public void run()
	    {LocalDateTime now = LocalDateTime.now();  
        System.out.println(now);
	        System.out.println("Thread is running.....");
	        try { 
				Thread.sleep(5);
				
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
	    }
	}


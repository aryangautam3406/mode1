/*
 * @author Aryan Gautam
 * Write a program to create a class DemoThread1 implementing Runnable interface.
 *  In the constructor, create a new thread and start the thread.
 *   In run() display a message "running child Thread in loop :"
 *  display the value of the counter ranging from 1 to 10. 
 *  Within the loop put the thread to sleep for 2 seconds.
 */


package hcl3;

public class DemoThread implements Runnable {

	public void run() {
		System.out.println("running child Thread in loop :");
		for (int i = 0; i <= 9; i++) {
			System.out.println(i);

			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

	public static void main(String args[]) {
		DemoThread m1 = new DemoThread();
		Thread t1 = new Thread(m1);
		t1.start();
	}
}

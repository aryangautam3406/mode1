package hcl2;

import java.time.LocalDateTime;

class ThreadNaming extends Thread {

	@Override
	public void run() {
		LocalDateTime now = LocalDateTime.now();
		System.out.println(now);
		System.out.println("Thread is running.....");

		try {
			Thread.sleep(5);  // Without try catch block : Unhandled Exception
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}
}
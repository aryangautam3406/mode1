/*
 * @author Aryan Gautam
 * 
 * In the previous program remove the try{}catch(){}
 *  block surrounding
 *  the sleep method and try to execute the code.
 *   What is your observation? 
 */

package hcl2;

public class ThreadMain {
	public static void main(String[] args) {

		ThreadNaming t1 = new ThreadNaming();
		ThreadNaming t2 = new ThreadNaming();

		// getting the above created threads names.
		System.out.println("Thread 1: " + t1.getName());
		System.out.println("Thread 2: " + t2.getName());

		t1.start();
		t2.start();

		t1.setName("Change1");
		t2.setName("Change2");

		System.out.println("Thread names after changing the " + "thread names");
		System.out.println("New Thread 1 name:  " + t1.getName());
		System.out.println("New Thread 2 name: " + t2.getName());

	}
}

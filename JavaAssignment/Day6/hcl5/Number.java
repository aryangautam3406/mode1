/*
 * @author Aryan Gautam
 * 
 *   
 *    Write a program to create a class Number which implements Runnable.
 *    and display multiples of a number.
 */




package hcl5;

public class Number implements Runnable {

	private int num=1;
	boolean j = true;
	private String t1;

	public void run() {
		System.out.println("running Thread in loop :");

	}

	public Number(int num, int num2) {
		while (j && num2 == 1) {
			j = false;
			for (int i = 0; i <= 9; i++)
				{this.num = num;
				this.num=this.num* num;}
			System.out.println(this.num);
		}
		j = true;
	}

	public static void main(String args[]) {

		Number m1 = new Number(2, 1);
		Number m2 = new Number(5, 1);
		Number m3 = new Number(8, 1);
		Thread t1 = new Thread(m1);
		Thread t2 = new Thread(m2);
		Thread t3 = new Thread(m3);

		t1.start();
		t2.start();
		t3.start();

	}
}

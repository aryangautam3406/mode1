@author Aryan_Gautam

2.Write a query to display the number of customer’s from Chennai. Give the count an alias name of Cust_Count.

CREATE table customer_master (customer_number Integer(15) primary key, First_Name Integer(10),Last_Name varchar(12));

Insert Into customer_master Values (123,'Ankit','Shukla');

Insert Into customer_master Values (312,'Rudra','Gautam');

Insert Into customer_master Values (758,'Kailash','Kher');

Insert Into customer_master Values (003,'Ram','Vilas');

Alter Table customer_master Add column City Varchar(12);

 UPDATE  customer_master
    -> SET City ='Shimla'
    -> Where customer_number = 123;

UPDATE  customer_master SET City = 'Lucknow' WHERE customer_number = 312;

UPDATE  customer_master SET City = 'Varanasi' WHERE customer_number = 003;

SELECT count(customer_number) as cust_count  FROM customer_master WHERE City='Chennai';

RESULT:
+------------+
| cust_count |
+------------+
|          1 |
+------------+




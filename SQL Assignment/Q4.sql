@author Aryan_Gautam
Write a query to display the number of customers who have registration but no account in the bank.
Give the alias name as Count_Customer for number of customers.

CREATE table account_master (account_number Integer(15) primary key, customer_number Integer(10),account_opening_date varchar(12));

INSERT INTO  account_master Values(123,758,'9 May 2021');

INSERT INTO  account_master Values(312,003,'9 May 2021');

INSERT INTO  account_master Values(758,099,'9 May 2021');

CREATE table customer_master (customer_number Integer(15) primary key, First_Name Integer(10),Last_Name varchar(12));

Insert Into customer_master Values (123,'Ankit','Shukla');

Insert Into customer_master Values (312,'Rudra','Gautam');

Insert Into customer_master Values (758,'Kailash','Kher');

Insert Into customer_master Values (003,'Ram','Vilas');

Alter Table customer_master Add column City Varchar(12);

 UPDATE  customer_master
    SET City ='Shimla'
    Where customer_number = 123;

UPDATE  customer_master SET City = 'Lucknow' WHERE customer_number = 312;

UPDATE  customer_master SET City = 'Varanasi' WHERE customer_number = 003;

Alter table account_master Modify column account_opening_date Integer(10);

UPDATE account_master SET account_opening_date = 15 where account_number = 123;

UPDATE account_master SET account_opening_date = 10 where account_number = 312;

INSERT INTO customer_master Values(004,'Narendra','Modi','Delhi');

SELECT count(customer_number) Count_Customer FROM customer_master WHERE customer_number NOT IN (SELECT customer_number FROM account_master);

Result:
+----------------+
| Count_Customer |
+----------------+
|              3 |
+----------------+
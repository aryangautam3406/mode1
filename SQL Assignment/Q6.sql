@author Aryan_Gautam
    
6.Write a query to display the number of clients who have asked for loans but they don’t have any account in the bank though they are registered customers. Give the count an alias name of Count.



CREATE table account_master (account_number Integer(15) primary key customer_number Integer(10),account_opening_date varchar(12));

INSERT INTO  account_master Values(123,758,'9 May 2021');

INSERT INTO  account_master Values(312,003,'9 May 2021');

INSERT INTO  account_master Values(758,099,'9 May 2021');

CREATE table customer_master (customer_number Integer(15) primary key, First_Name Integer(10),Last_Name varchar(12));

Insert Into customer_master Values (123,'Ankit','Shukla');

Insert Into customer_master Values (312,'Rudra','Gautam');

Insert Into customer_master Values (758,'Kailash','Kher');

Insert Into customer_master Values (003,'Ram','Vilas');

Alter Table customer_master Add column City Varchar(12);

 UPDATE  customer_master
    SET City ='Shimla'
    Where customer_number = 123;

UPDATE  customer_master SET City = 'Lucknow' WHERE customer_number = 312;

UPDATE  customer_master SET City = 'Varanasi' WHERE customer_number = 003;

Alter table account_master Modify column account_opening_date Integer(10);

UPDATE account_master SET account_opening_date = 15 where account_number = 123;

UPDATE account_master SET account_opening_date = 10 where account_number = 312;

INSERT INTO customer_master Values(004,'Narendra','Modi','Delhi');

Alter table customer_master Drop Primary key;

Alter table customer_master Add column customer_id Integer(5) Auto_Increment primary key;

INSERT INTO customer_master Values(758,'Rahul','Gandhi','Wayanad',6);

INSERT INTO customer_master Values(099,'Sonia','Gandhi','Amethi',7);

INSERT INTO customer_master Values(1047,'Amit','Shah','Chennai',8);

INSERT INTO account_master Values(923,758,8);

INSERT INTO account_master Values(947,099,18);

INSERT INTO account_master Values(1047,758,1);

Update customer_master SET customer_number = 786 where First_name = 'Rahul';

select First_Name FROM customer_master cm INNER JOIN account_master am ON cm.customer_number = am.customer_number group by First_Name having count(account_number) > 1 order by First_Name;

Result:
+------------+
| First_Name |
+------------+
| Kailash    |
| Sonia      |
+------------+
1. Write a query to display account number, customer’s number, customer’s firstname,lastname,account opening date.
Display the records sorted in ascending order based on account number.

CREATE table account_master (account_number Integer(15) primary key, customer_number Integer(10),account_opening_date varchar(12));

INSERT INTO  account_master Values(123,758,'9 May 2021');

INSERT INTO  account_master Values(312,003,'9 May 2021');

INSERT INTO  account_master Values(758,099,'9 May 2021');

CREATE table customer_master (customer_number Integer(15) primary key, First_Name Integer(10),Last_Name varchar(12));

Insert Into customer_master Values (123,'Ankit','Shukla');

Insert Into customer_master Values (312,'Rudra','Gautam');

Insert Into customer_master Values (758,'Kailash','Kher');

Insert Into customer_master Values (003,'Ram','Vilas');

SELECT account_number,am.customer_number,First_Name,Last_Name,account_opening_date FROM customer_master cm INNER JOIN account_master am ON cm.customer_number=am.customer_number ORDER BY account_number;

RESULT:
+----------------+-----------------+------------+-----------+----------------------+
| account_number | customer_number | First_Name | Last_Name | account_opening_date |
+----------------+-----------------+------------+-----------+----------------------+
|            123          |             758                | Kailash      | Kher           | 9 May 2021           |
|            312          |               3                  | Ram          | Vilas           | 9 May 2021           |
+----------------+-----------------+------------+-----------+----------------------+



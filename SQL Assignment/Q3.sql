CREATE table account_master (account_number Integer(15) primary key, customer_number Integer(10),account_opening_date varchar(12));

INSERT INTO  account_master Values(123,758,'9 May 2021');

INSERT INTO  account_master Values(312,003,'9 May 2021');

INSERT INTO  account_master Values(758,099,'9 May 2021');

CREATE table customer_master (customer_number Integer(15) primary key, First_Name Integer(10),Last_Name varchar(12));

Insert Into customer_master Values (123,'Ankit','Shukla');

Insert Into customer_master Values (312,'Rudra','Gautam');

Insert Into customer_master Values (758,'Kailash','Kher');

Insert Into customer_master Values (003,'Ram','Vilas');

Alter Table customer_master Add column City Varchar(12);

 UPDATE  customer_master
    -> SET City ='Shimla'
    -> Where customer_number = 123;

UPDATE  customer_master SET City = 'Lucknow' WHERE customer_number = 312;

UPDATE  customer_master SET City = 'Varanasi' WHERE customer_number = 003;

Alter table account_master Modify column account_opening_date Integer(10);

UPDATE account_master SET account_opening_date = 15 where account_number = 123;

UPDATE account_master SET account_opening_date = 10 where account_number = 312;

UPDATE account_master SET account_opening_date = 15 where account_number = 758;

SELECT am.customer_number, First_Name, account_number FROM customer_master cm INNER JOIN account_master am ON cm.customer_number = am.customer_number WHERE account_opening_date > 10 ORDER BY am.customer_number, account_number;

RESULT: +-----------------+------------+----------------+
| customer_number | First_Name | account_number |
+-----------------+------------+----------------+
|             758            | Kailash          |               123 |
+-----------------+------------+----------------+

package com.dao;

import java.util.List;

import com.model.Pet;
import com.model.User;

public interface UserDao {
public abstract User SaveUser(User user); 
public abstract Boolean userDaoauthenticateUser(User user);
public abstract Pet AddPet(Pet pet);
public abstract List<Pet> ViewPet(Pet pet);
public abstract Pet BuyPet(int petid,Pet pet,User user);


}

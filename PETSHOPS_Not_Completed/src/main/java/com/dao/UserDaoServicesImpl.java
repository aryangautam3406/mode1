package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import com.model.Pet;
import com.model.User;


@Repository
public class UserDaoServicesImpl implements UserDao {
	  @Autowired
	   	private SessionFactory sessionFactory;
	   	
	   	
	   	public SessionFactory getSessionFactory() {
	   			return sessionFactory;
	   		}

	   		public void setSessionFactory(SessionFactory sessionFactory) {
	   			this.sessionFactory = sessionFactory;
	   		}
	       
	@Override
	public User SaveUser(User user) {
	
		
		Session session = this.sessionFactory.getCurrentSession();
		


		Query query = session.createQuery("FROM User e WHERE e.USERNAME= ?0");
		query.setParameter(0, user.getUSERNAME());
		List<User> results = query.getResultList();
	
      if(results.isEmpty()) {
	 
	       session.save(user);
	        return user;
          }
 else {
	
	 user.setUSERNAME(null);
 }

	return user;
		}


	@Override
	public Boolean userDaoauthenticateUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		


		Query query = session.createQuery("FROM User e WHERE e.USERNAME= ?0 and e.USERPASSWORD= ?1");
		query.setParameter(0, user.getUSERNAME());
		query.setParameter(1, user.getUSERPASSWORD());
		List<User> results=query.getResultList();
		
		if(results.isEmpty())
		{
			
			return false;
		}
			
			
		else
		{Query query1 = session.createQuery("FROM User e WHERE e.USERNAME= ?0 ");
		query1.setParameter(0, user.getUSERNAME());
		List<User> results1=query1.getResultList();
		for (User user2 : results1) {
			user.setUSERID(user2.getUSERID());
		}
			return true;
			
		}
			

		
	}

	@Override
	public Pet AddPet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		
		session.save(pet);
		Query query = session.createQuery("FROM Pet");
		List<Pet> results=query.getResultList();
		
		for (Pet pet2 : results) {
			System.out.println(pet2.getPETNAME());
		}
		return null;
	}

	@Override
	public List<Pet> ViewPet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM Pet ");
		List<Pet> results=query.getResultList();
		for (Pet pet2 : results) {
			System.out.println(pet2.getPETNAME());
		}
		
		return results;
	}

	@Override
	public Pet BuyPet(int petid, Pet pet,User user) {
		
        Session session = this.sessionFactory.getCurrentSession();
		
		//Query query=session.createQuery("UPDATE Pet set user = ?1 "  + 
          //                             "WHERE PETID = ?0");
        
      //  Query query=session.createQuery("fROM Pet p where p.PETID=?0 ");
		
		//Query query=session.createQuery("FROM Pet where PETID=?0 "+"INSERT INTO Pet(user)"+"Select USERID from User u Where u.USERID=?1");
		//Query query = session.createQuery("INSERT INTO   Pet (user) select From User u where u.USERID=1");
        Query query = session.createQuery("update Pet set user =?0 where PETID=?1");
        
		query.setParameter(0, user);
		query.setParameter(1, petid);
	int i=	query.executeUpdate();
	System.out.println(i);
         /* List<Pet> result=query.getResultList();
		 for (Pet object : result) {
		object.setPet(user);;
		session.save(object);
		}*/
		
		return null;
	}
}
		


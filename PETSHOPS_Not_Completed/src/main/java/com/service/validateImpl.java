package com.service;

import java.util.List;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.model.Pet;
import com.model.User;





@Service
public class validateImpl implements UserValidate{
       @Autowired
	private UserDao userdao;
       
       
     
	@Transactional
	@Override
	public User SaveUser(User user) {
		
		System.out.println(user.getUSERNAME());
		user=userdao.SaveUser(user);
		return user;
	}
    @Transactional
	@Override
	public Boolean AuthenticateUser(User user) {
		System.out.println(user.getUSERNAME());
		
		Boolean check=userdao.userDaoauthenticateUser(user);
		return check;
	}
    @Transactional
	@Override
	public Pet AddPet(Pet pet) {
		Pet petlist=userdao.AddPet(pet);
		return null;
	}
	@Override
	 @Transactional
	public List<Pet> ViewPet(Pet pet) {
		List<Pet> petlist = userdao.ViewPet(pet);
		return petlist;
	}
	@Override
	@Transactional
	public Pet BuyPet(int petid, Pet pet,User user) {
		 userdao.BuyPet(petid, pet, user);
		return null;
	}

}

package com.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

   


@Entity
@Table(name="user")
public class User implements Serializable{
@Id @GeneratedValue(strategy=GenerationType.AUTO)
private int USERID;
@NotEmpty
private String USERNAME;
@NotEmpty
private String USERPASSWORD;

@OneToMany(mappedBy="user" ,cascade = {CascadeType.ALL})
private List<Pet> pet;



public List<Pet> getPet() {
	return pet;
}
public void setPet(List<Pet> pet) {
	this.pet = pet;
}
public User(int uSERID, String uSERNAME, String uSERPASSWORD) {
	super();
	USERID = uSERID;
	USERNAME = uSERNAME;
	USERPASSWORD = uSERPASSWORD;
}
public User() {
	super();

}
public int getUSERID() {
	return USERID;
}
public void setUSERID(int uSERID) {
	USERID = uSERID;
}
public String getUSERNAME() {
	return USERNAME;
}
public void setUSERNAME(String uSERNAME) {
	USERNAME = uSERNAME;
}
public String getUSERPASSWORD() {
	return USERPASSWORD;
}
public void setUSERPASSWORD(String uSERPASSWORD) {
	USERPASSWORD = uSERPASSWORD;
}


}

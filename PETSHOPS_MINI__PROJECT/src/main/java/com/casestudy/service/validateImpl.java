package com.casestudy.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.casestudy.dao.UserDao;
import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Service
public class validateImpl implements UserValidate {
	@Autowired
	private UserDao userdao;

	@Transactional
	@Override
	public User saveUser(User user) {
		User users = userdao.saveUser(user);
		return users;
	}

	@Transactional
	@Override
	public User authenticateUser(User user) {
		System.out.println(user.getUserName());

		User check = userdao.userDaoauthenticateUser(user);
		return check;
	}

	@Transactional
	@Override
	public Pet addPet(Pet pet) {
		userdao.addPet(pet);
		return null;
	}

	@Override
	@Transactional
	public List<Pet> getAllPets(Pet pet) {
		List<Pet> petlist = userdao.getAllPets(pet);
		return petlist;
	}

	@Override
	@Transactional
	public Pet buyPet(int petid, long userid) {
		System.out.println(userid);
		userdao.buyPet(petid, userid);
		return null;
	}

	@Override
	@Transactional
	public List<Pet> getMyPets(long userid) {
		List<Pet> petlist = userdao.getMyPets(userid);
		return petlist;
	}

}

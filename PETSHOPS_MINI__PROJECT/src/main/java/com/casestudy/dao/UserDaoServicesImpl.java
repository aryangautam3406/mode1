package com.casestudy.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

@Repository
public class UserDaoServicesImpl implements UserDao {

	private static final Logger logger = LogManager.getLogger(UserDaoServicesImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User saveUser(User user) {

		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery("FROM User e WHERE e.userName= ?0");
		query.setParameter(0, user.getUserName());
		List<User> results = query.getResultList();

		if (results.isEmpty()) {

			session.save(user);
			return user;
		}

		else {

			user.setUserName(null);
		}

		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public User userDaoauthenticateUser(User user) {
		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery("FROM User e WHERE e.userName= ?0 and e.userPassword= ?1");
		query.setParameter(0, user.getUserName());
		query.setParameter(1, user.getUserPassword());

		List<User> results = query.getResultList();

		if (results.isEmpty()) {

			return user;
		}

		else {

			for (User user2 : results) {
				logger.info(user2.getUserId());
				Long userid = user2.getUserId();
				user.setUserId(userid);
			}

		}

		return user;
	}

	@Override
	public Pet addPet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();

		session.save(pet);

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pet> getAllPets(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery("FROM Pet where PETOWNERID=null ");
		List<Pet> results = query.getResultList();
		for (Pet pet2 : results) {
			logger.info(pet2.getPetName());
		}

		return results;
	}

	@Override
	public Pet buyPet(int petid, long userid) {

		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery("update Pet set PETOWNERID = ?0 where petId = ?1");
		logger.info(userid);
		logger.info(petid);
		query.setParameter(0, userid);
		query.setParameter(1, petid);
		int i = query.executeUpdate();
		logger.info("Rows updated" + i);

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pet> getMyPets(long userid) {

		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery(" From Pet  where (PETOWNERID =?0) ");

		query.setParameter(0, userid);

		List<Pet> results = query.getResultList();
		for (Pet pet2 : results) {
			logger.info(pet2.getPetName());
		}

		return results;

	}

}

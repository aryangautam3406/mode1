package com.casestudy.dao;

import java.util.List;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

public interface UserDao {
public abstract User saveUser(User user); 
public abstract User userDaoauthenticateUser(User user);
public abstract Pet addPet(Pet pet);
public abstract List<Pet> getAllPets(Pet pet);
public abstract Pet buyPet(int petid,long userid);
public abstract List<Pet> getMyPets(long userid);

}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HOME</title>
<style>
<
style>.error {
	color: red;
}

.tab2 {
	border: 2px solid green;
	background-color: #FF5733;
	margin-left: 45%;
}

.tab {
	height: 53px;
	border: 4px solid green;
	background-color: grey;
}

.tablinks {
	background-color: grey;
	float: right;
	border: none;
	cursor: pointer;
	font-size: 16px;
	padding-top: 6px;
	padding-bottom: 10px;
	padding-right: 10px;
}

.tablinks2 {
	background-color: grey;
	float: left;
	border: none;
	cursor: pointer;
	font-size: 16px;
	padding-top: 6px;
	padding-bottom: 10px;
	padding-right: 10px;
}

h3 {
	float: right;
	background-color: orange;
}
</style>

</head>

<body>

	<div class="tab">
		<div>
			<form action="logout" class="tablinks" method="GET">
				<button class="float-left submit-button">Logout</button>
			</form>


			<form action="" class="tablinks2" method="GET">
				<button class="float-left submit-button">PETSHOP</button>
			</form>
			<form action="home" class="tablinks2" method="GET">
				<button class="float-left submit-button">Home</button>
			</form>
			<form action="" class="tablinks2" method="GET">
				<button class="float-left submit-button">AddPet</button>
			</form>
			<form action="myPets" class="tablinks2" method="GET">
				<button class="float-left submit-button">MyPet</button>
			</form>
			<h3>Welcome ${username }</h3>
		</div>
	</div>

	<table class="tab2">
		<tr>
			<td><h2>Fill Pet Details</h2></td>
		</tr>
		<spring:form action="addpets" modelAttribute="Pet" method="Get">

			<tr>
				<td><label>PETNAME</label>
			<tr>
				<td>
			<tr>
				<td><spring:input path="petName" /></td>
			</tr>
			<br>
			<tr>
				<td><spring:errors class="error" path="petName"></spring:errors><br>
			<tr>
				<td><label>PETAGE</label></td>
			</tr>
			<tr>
				<td><spring:input path="petAge" /></td>
			</tr>
			<br>
			<tr>
				<td><spring:errors class="error" path="petAge"></spring:errors></td>
			</tr>
			<br>

			<tr>
				<td><label>PETPLACE</label>
			<tr>
				<td><spring:input path="petPlace" /><br>
			<tr>
				<td><spring:errors class="error" path="petPlace"></spring:errors></td>
			</tr>
			<br>



			<tr>
				<td><input type="submit" value="submit"></td>
			</tr>

		</spring:form>
	</table>
</body>
</html>
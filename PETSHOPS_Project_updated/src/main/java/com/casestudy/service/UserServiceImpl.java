package com.casestudy.service;



import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.casestudy.dao.UserDao;

import com.casestudy.model.User;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userdao;

	@Transactional
	@Override
	public User saveUser(User user) {
		User users = userdao.saveUser(user);
		return users;
	}

	@Transactional
	@Override
	public User authenticateUser(User user) {
		System.out.println(user.getUserName());

		User check = userdao.userDaoauthenticateUser(user);
		return check;
	}


}

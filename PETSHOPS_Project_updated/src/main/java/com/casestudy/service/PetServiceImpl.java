package com.casestudy.service;

import java.util.List;

import javax.transaction.Transactional;

import com.casestudy.dao.PetDao;
import com.casestudy.model.Pet;

public class PetServiceImpl implements PetService{
	
	private PetDao petdao;
	
	@Transactional
	@Override
	public Pet addPet(Pet pet) {
		petdao.addPet(pet);
		return null;
	}

	@Override
	@Transactional
	public List<Pet> getAllPets(Pet pet) {
		List<Pet> petlist = petdao.getAllPets(pet);
		return petlist;
	}

	@Override
	@Transactional
	public Pet buyPet(int petid, long userid) {
		System.out.println(userid);
		petdao.buyPet(petid, userid);
		return null;
	}

	@Override
	@Transactional
	public List<Pet> getMyPets(long userid) {
		List<Pet> petlist = petdao.getMyPets(userid);
		return petlist;
	}

}

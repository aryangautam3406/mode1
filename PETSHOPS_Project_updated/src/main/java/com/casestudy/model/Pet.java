package com.casestudy.model;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.springframework.stereotype.Component;


@Component
@Entity
@Table(name="pet")
public class Pet implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int petId;
	@NotEmpty(message = "Please provide Name Of Pet")
	private String petName;
	@NotEmpty(message = "Please provide Age Of Pet")
	@Min(0) @Max(99)
	private String petAge;
	@NotEmpty(message = "Please provide Pet Place")
	private String petPlace;
	@ManyToOne
	@JoinColumn(name="PETOWNERID")
	private User userID;
	
	
	
	
	
	public Pet() {
		super();
		
	}
	public Pet(int petId, @NotEmpty String petName, @NotEmpty String petAge, @NotEmpty String petPlace, User userID) {
		super();
		this.petId = petId;
		this.petName = petName;
		this.petAge = petAge;
		this.petPlace = petPlace;
		this.userID = userID;
	}
	public int getPetId() {
		return petId;
	}
	public void setPetId(int petId) {
		this.petId = petId;
	}
	public String getPetName() {
		return petName;
	}
	public void setPetName(String petName) {
		this.petName = petName;
	}
	public String getPetAge() {
		return petAge;
	}
	public void setPetAge(String petAge) {
		this.petAge = petAge;
	}
	public String getPetPlace() {
		return petPlace;
	}
	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}
	public User getUserID() {
		return userID;
	}
	public void setUserID(User userID) {
		this.userID = userID;
	}
	
	
	
	
}

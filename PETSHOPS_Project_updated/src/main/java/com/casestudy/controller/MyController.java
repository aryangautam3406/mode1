package com.casestudy.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.PetService;
import com.casestudy.service.UserService;

@Controller
public class MyController {
	private static final Logger logger = LogManager.getLogger(MyController.class);

	@Autowired
	private UserService userservice;
	@Autowired
	private PetService petservice;
	@Autowired
	private User user;

	@GetMapping(value = "login")
	public ModelAndView login(User user) {
		ModelAndView modelAndView = new ModelAndView("loginpage");
		modelAndView.addObject("User", user);

		return modelAndView;
	}

	@GetMapping(value = "home")
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("home");

		List<Pet> petlist = petservice.getAllPets(null);
		logger.info(user.getUserName());
		modelAndView.addObject("username", user.getUserName());
		modelAndView.addObject("petlist", petlist);
		modelAndView.setViewName("home");
		return modelAndView;
	}

	@GetMapping(value = "addpet")
	public ModelAndView addPet(Pet pet) {
		ModelAndView modelAndView = new ModelAndView("addpet");
		modelAndView.addObject("Pet", pet);
		modelAndView.addObject("username", user.getUserName());
		return modelAndView;
	}

	@GetMapping(value = "myPets")
	public void myPets(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info(user.getUserId());
		List<Pet> petlist = petservice.getMyPets(user.getUserId());
		request.setAttribute("petlist", petlist);
		request.setAttribute("username", user.getUserName());
		RequestDispatcher rd = request.getRequestDispatcher("mypets.jsp");
		rd.forward(request, response);
	}

	@GetMapping(value = "buy")
	public ModelAndView buypet(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		String id = request.getParameter("petid");
		int petid = Integer.parseInt(id);
		logger.info(user.getUserId());
		logger.info(petid);
		logger.info("petid Parsed");
		logger.info(user.getUserId());
		long userid = user.getUserId();
		petservice.buyPet(petid, userid);
		List<Pet> petlist = petservice.getAllPets(null);
		modelAndView.addObject("petlist", petlist);
		modelAndView.addObject("username", user.getUserName());
		modelAndView.setViewName("home");
		return modelAndView;
	}

	@GetMapping(value = "loginuser")
	public ModelAndView authenticateUser(@ModelAttribute("User") User users, HttpServletRequest request)
			throws IOException {

		logger.info(users.getUserName());
		ModelAndView modelAndView = new ModelAndView("loginpage");

		User check = userservice.authenticateUser(users);
		if (check.getUserId() != 0) {
			logger.info(check.getUserId());
			user.setUserId(check.getUserId());
			user.setUserName(check.getUserName());
			logger.info(user.getUserId());
			logger.info(user.getUserName());
			List<Pet> petlist = petservice.getAllPets(null);
			request.getSession(true);
			modelAndView.addObject("petlist", petlist);
			modelAndView.addObject("username", user.getUserName());
			modelAndView.setViewName("home");
			return modelAndView;
		} else {
			modelAndView.addObject("error", "Wrong Credentials");
			modelAndView.setViewName("loginpage");
			return modelAndView;
		}

	}

	@GetMapping(value = "addpets")
	public ModelAndView savePet(@Valid @ModelAttribute("Pet") Pet pet, BindingResult bindingResult) throws IOException {
		ModelAndView modelAndView = new ModelAndView();
		logger.info(pet.getPetName());

		if (bindingResult.hasErrors()) {

			modelAndView.setViewName("addpet");

		} else {
			petservice.addPet(pet);
			modelAndView.addObject("username", user.getUserName());
			List<Pet> petlist = petservice.getAllPets(null);

			modelAndView.addObject("petlist", petlist);
			modelAndView.setViewName("home");

		}

		return modelAndView;
	}

	@GetMapping(value = "save")
	public ModelAndView saveUser(@ModelAttribute(value = "User") User user) throws IOException {

		ModelAndView modelAndView = new ModelAndView();

		if (user.getUserPassword().equals(user.getConfirmPassword())) {
			logger.info("Password checked");
			logger.info(user.getUserName());

			User check = userservice.saveUser(user);

			if (check.getUserName() == null) {
				modelAndView.addObject("key1", "Try With Different Name");
				modelAndView.setViewName("registrationpage");
				return modelAndView;
			}

			else {
				modelAndView.addObject("key", "SuccessFull Registration Now Please Login");
				modelAndView.setViewName("loginpage");
				return modelAndView;
			}

		}

		else {
			modelAndView.addObject("key", "Password And Confirm Password Must Be same");
			modelAndView.setViewName("registrationpage");
			return modelAndView;
		}

	}

	@GetMapping(value = "logout")
	public String logoutPage(HttpServletRequest request) {
		request.getSession(false).invalidate();
		return "loginpage";
	}

}

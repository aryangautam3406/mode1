package com.casestudy.dao;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.casestudy.controller.MyController;
import com.casestudy.model.Pet;

public class PetDaoImpl implements PetDao {

	private static final Logger logger = LogManager.getLogger(MyController.class);
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Pet addPet(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();

		session.save(pet);

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pet> getAllPets(Pet pet) {
		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery("FROM Pet where PETOWNERID=null ");
		List<Pet> results = query.getResultList();
		for (Pet pet2 : results) {
			logger.info(pet2.getPetName());
		}

		return results;
	}

	@Override
	public Pet buyPet(int petid, long userid) {

		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery("update Pet set PETOWNERID = ?0 where petId = ?1");
		logger.info(userid);
		logger.info(petid);
		query.setParameter(0, userid);
		query.setParameter(1, petid);
		int i = query.executeUpdate();
		logger.info("Rows updated" + i);

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pet> getMyPets(long userid) {

		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery(" From Pet  where (PETOWNERID =?0) ");

		query.setParameter(0, userid);

		List<Pet> results = query.getResultList();
		for (Pet pet2 : results) {
			logger.info(pet2.getPetName());
		}

		return results;

	}

}

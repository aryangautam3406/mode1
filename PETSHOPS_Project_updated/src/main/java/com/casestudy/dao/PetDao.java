package com.casestudy.dao;

import java.util.List;

import com.casestudy.model.Pet;

public interface PetDao {
	public abstract List<Pet> getAllPets(Pet pet);

	public abstract Pet addPet(Pet pet);

	public abstract Pet buyPet(int petid, long userid);

	public abstract List<Pet> getMyPets(long userid);
}

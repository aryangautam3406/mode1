<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<style>
.error, h5 {
	color: red;
}

.tab {
	height: 53px;
	border: 4px solid green;
	background-color: grey;
}

.tablinks {
	background-color: grey;
	float: right;
	border: none;
	cursor: pointer;
	font-size: 16px;
	padding-top: 6px;
	padding-bottom: 10px;
	padding-right: 10px;
}

.tablinks2 {
	background-color: grey;
	float: left;
	border: none;
	cursor: pointer;
	font-size: 16px;
	padding-top: 6px;
	padding-bottom: 10px;
	padding-right: 10px;
}

.table1 {
	height: 12;
}
</style>


</head>
<body>

	<div class="tab">
		<div>
			<form action="login" class="tablinks" method="GET">
				<button class="float-left submit-button">Login</button>
			</form>

			<form action="" class="tablinks2" method="GET">
				<button class="float-left submit-button">PETSHOP</button>
			</form>
		</div>
	</div>





	<form action="save" modelAttribute="User" method="Get">

		<div>
			<h2>Registration</h2>
		</div>
		<table>
			<div class="table1">
				<tr>
					<td><LABEL>NAME</LABEL></td>
				</tr>
				<tr>
					<td><input name="userName" required /></td>
				<tr>
				<tr>
					<td><h5>${key1}</h5></td>
				<tr>


					<div class="table1">

						<tr>
							<td><LABEL>PASSWORD</LABEL></td>
						<tr>
						<tr>
							<td><input name="userPassword" required /></td>
						<tr>
						<tr>
							<td><LABEL>CONFIRM PASSWORD</LABEL></td>
						<tr>
						<tr>
							<td><input name="confirmPassword" required /></td>
						<tr>
						<tr>
							<td><h5>${key}</h5></td>
						<tr>
						<tr>
							<td><input type="submit" value="Register"></td>
						<tr>
		</table>
	</form>
</body>
</html>
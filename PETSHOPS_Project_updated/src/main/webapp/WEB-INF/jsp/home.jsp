<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HOME</title>
<style>
.tab2 {
	border: 2px solid green;
	background-color: #FF5733;
	margin-left: 45%;
}

.tab {
	height: 53px;
	border: 4px solid green;
	background-color: grey;
}

.tablinks {
	background-color: grey;
	float: right;
	border: none;
	cursor: pointer;
	font-size: 16px;
	padding-top: 6px;
	padding-bottom: 10px;
	padding-right: 10px;
}

.tablinks2 {
	background-color: grey;
	float: left;
	border: none;
	cursor: pointer;
	font-size: 16px;
	padding-top: 6px;
	padding-bottom: 10px;
	padding-right: 10px;
}

h3 {
	float: right;
	background-color: orange;
}
</style>







</head>
<body>




	<div class="tab">
		<div>
			<form action="logout" class="tablinks" method="GET">
				<button class="float-left submit-button">Logout</button>
			</form>


			<form action="" class="tablinks2" method="GET">
				<button class="float-left submit-button">PETSHOP</button>
			</form>
			<form action="" class="tablinks2" method="GET">
				<button class="float-left submit-button">Home</button>
			</form>
			<form action="addpet" class="tablinks2" method="GET">
				<button class="float-left submit-button">AddPet</button>
			</form>
			<form action="myPets" class="tablinks2" method="GET">
				<button class="float-left submit-button">MyPet</button>
			</form>
			<h3>Welcome ${username }</h3>
		</div>
	</div>








	<table class="tab2">
		<thread>
		<tr>
			<th>Pet Name</th>
			<th>Pet Age</th>
			<th>Pet Place</th>
			<th>Current Status</th>
		</tr>

		</thread>
		<tbody>
			<c:forEach var="petlist" items="${petlist}">
				<tr>
					<td><c:out value="${petlist.petName}" /></td>
					<td><c:out value="${petlist.petAge}" /></td>
					<td><c:out value="${petlist.petPlace}" /></td>
					<td><a href="buy?petid=<c:out value='${petlist.petId}'/>">BUY</a>
					</td>
				</tr>
			</c:forEach>
	</table>
	</tbody>







</body>
</html>